﻿** Settings ***
Library         Collections
Library         String
Library         RequestsLibrary
Library         json
Library         JSONLibrary
Library         urllib3
Library         BuiltIn
Library         OperatingSystem
Library         DateTime


*** Variables ***
${base_url_snp}         https://skt-api-dev.snp1344.com
${email}                testapi@mail.com
${apikey}               test1234
${jobType}              3
${option}               3
${promoCode}            NT4U
${skootarId}            SP6060
${address_detail_1}     addressName=สปริงเรสสิเดนท์  address=26 ถ. ลาดพร้าว แขวง สะพานสอง เขตวังทองหลาง กรุงเทพมหานคร 10310 ประเทศไทยnullกรุงเทพมหานครประเทศไทย10310  lat=13.69393102997744  lng=100.6017630547285  contactName=Ms. One  contactPhone=0968768177  cashFee=N  seq=1
${address_detail_2}     addressName=อาคารปิยะเพลส ชั้น10 ห้อง10อี เคียวอุเดาะ  address=29 ถนน หลังสวน แขวง ลุมพินี เขตปทุมวัน กรุงเทพมหานคร 10330 ประเทศไทย  lat=13.67891465415223  lng=100.61369150877  contactName=Ms. Two  contactPhone=0952483969  cashFee=Y  seq=2
${branchId}             1212

*** Test Cases ***
PREPARE_DATA
    ${RANDOM_LAT}   Generate random string    14    0123456789
    ${RANDOM_LNG}   Generate random string    14    0123456789
    ${TODAY} =       Get Current Date
    Set Global Variable                          ${TODAY}
    Set Global Variable                          ${RANDOM_LAT}
    Set Global Variable                          ${RANDOM_LNG}


ESTIMATE_PRICE
    ${loc1}=            Create Dictionary       addressName=สปริงเรสสิเดนท์  address=26 ถ. ลาดพร้าว แขวง สะพานสอง เขตวังทองหลาง กรุงเทพมหานคร 10310 ประเทศไทยnullกรุงเทพมหานครประเทศไทย10310  lat=13.69393102997744  lng=100.6017630547285  contactName=Ms. One  contactPhone=0968768177  cashFee=N  seq=1
    ${loc2}=            Create Dictionary       addressName=อาคารปิยะเพลส ชั้น10 ห้อง10อี เคียวอุเดาะ  address=29 ถนน หลังสวน แขวง ลุมพินี เขตปทุมวัน กรุงเทพมหานคร 10330 ประเทศไทย  lat=13.67891465415223  lng=100.61369150877  contactName=Ms. Two  contactPhone=0952483969  cashFee=Y  seq=2
    ${locationList}=    Create List             ${loc1}    ${loc2}
    Create Session      estimate_price          ${base_url_snp}         verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${body}=            Create dictionary       userName=${email}       apiKey=${apikey}  jobType=${jobType}  option=${option}  promoCode=${promoCode}  locationList=${locationList}
    ${response}=        POST On Session         estimate_price          /skootar_api_dev/api/get_estimate_price       headers=${headers}      json=${body}
    ${response.status_code}                     Convert To String       ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}                     200
    ${responseCode}=                            Evaluate    $response.json().get("responseCode")
    should be equal                             ${responseCode}                             200


TC1_CREATE_NEW_JOB_FIXED_SKOOTAR
    ${loc1}=            Create Dictionary       addressName=สปริงเรสสิเดนท์  address=26 ถ. ลาดพร้าว แขวง สะพานสอง เขตวังทองหลาง กรุงเทพมหานคร 10310 ประเทศไทยnullกรุงเทพมหานครประเทศไทย10310  lat=13.69393102997744  lng=100.6017630547285  contactName=Ms. One  contactPhone=0968768177  seq=1
    ${loc2}=            Create Dictionary       addressName=อาคารปิยะเพลส ชั้น10 ห้อง10อี เคียวอุเดาะ  address=29 ถนน หลังสวน แขวง ลุมพินี เขตปทุมวัน กรุงเทพมหานคร 10330 ประเทศไทย  lat=13.67891465415223  lng=100.61369150877  contactName=Ms. Two  contactPhone=0952483969  seq=2
    ${locationList}=    Create List             ${loc1}    ${loc2}
    Create Session      create_new_job          ${base_url_snp}                     verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${body}=            Create dictionary       userName=${email}  apiKey=${apikey}  fixedSkootarId=${skootarId}  jobDate=${TODAY}  startTime=now  finishTime=null  jobType=${jobType}  option=${option}  refNo=ROBOT-${TODAY}  remark=Robotframework Test Api  callbackUrl=https://release.skootar.com/api/admin/test/callback  locationList=${locationList}
    ${response}=        POST On Session         create_new_job                      /skootar_api_dev/api/create_new_job       headers=${headers}      json=${body}
    ${response.status_code}                     Convert To String                   ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    #log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}             200
    ${responseCode}=                            evaluate                            $response.json().get("responseCode")
    should be equal                             ${responseCode}                     200
    ${jobId}=                                   evaluate                            $response.json().get("jobDetail").get("jobId")
    log to console                              \n==============================================================================\n
    log to console                              CREATED JOB ID : ${jobId}
    Set Global Variable                         ${jobId}


TC1_DRIVER_ACCEPT_MISSION
    Create Session      accept_mission          ${base_url_snp}                      verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${response}=        POST On Session         accept_mission                      /skootar_ws_dev_v1/android_ws/v2/mission/accept/${jobId}/${skootarId}/1/1/13.${RANDOM_LAT}/100.${RANDOM_LNG}       headers=${headers}
    ${response.status_code}                     Convert To String                   ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    log to console                              body : ${response.content}
    ${responseCode}=                            evaluate                            $response.json().get("code")
    ${responseCode}                             Convert To String                   ${responseCode}
    ${responsedescTH}=                          evaluate                            $response.json().get("desc")
    should be equal                             ${responseCode}                     200
    should be equal                             ${responsedescTH}                   รับงานสำเร็จ


TC1_DRIVER_COMPLETE_MISSION
    Create Session      complete_mission        ${base_url_snp}                      verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${response}=        POST On Session         complete_mission                    /skootar_ws_dev_v1/android_ws/v3/mission/complete/${jobId}/${skootarId}/1/13.${RANDOM_LAT}/100.${RANDOM_LNG}/0       headers=${headers}
    ${response.status_code}                     Convert To String                   ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    log to console                              body : ${response.content}
    ${responseCode}=                            evaluate                            $response.json().get("code")
    ${responseCode}                             Convert To String                   ${responseCode}
    ${responsedescTH}=                          evaluate                            $response.json().get("desc")
    should be equal                             ${responseCode}                     200
    should be equal                             ${responsedescTH}                   ปิดงานเรียบร้อยแล้ว


TC2_CREATE_NEW_JOB_BRANCH
    ${loc1}=            Create Dictionary       addressName=สปริงเรสสิเดนท์  address=26 ถ. ลาดพร้าว แขวง สะพานสอง เขตวังทองหลาง กรุงเทพมหานคร 10310 ประเทศไทยnullกรุงเทพมหานครประเทศไทย10310  lat=13.69393102997744  lng=100.6017630547285  contactName=Ms. One  contactPhone=0968768177  seq=1
    ${loc2}=            Create Dictionary       addressName=อาคารปิยะเพลส ชั้น10 ห้อง10อี เคียวอุเดาะ  address=29 ถนน หลังสวน แขวง ลุมพินี เขตปทุมวัน กรุงเทพมหานคร 10330 ประเทศไทย  lat=13.67891465415223  lng=100.61369150877  contactName=Ms. Two  contactPhone=0952483969  seq=2
    ${locationList}=    Create List             ${loc1}    ${loc2}
    Create Session      create_new_job          ${base_url_snp}                     verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${body}=            Create dictionary       userName=${email}  apiKey=${apikey}  branchId=${branchId}  jobDate=${TODAY}  startTime=now  finishTime=null  jobType=${jobType}  option=${option}  refNo=ROBOT-${TODAY}2  remark=Robotframework Test Api callbackUrl=https://release.skootar.com/api/admin/test/callback  locationList=${locationList}
    ${response}=        POST On Session         create_new_job                      /skootar_api_dev/api/create_new_job       headers=${headers}      json=${body}
    ${response.status_code}                     Convert To String                   ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    #log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}             200
    ${responseCode}=                            evaluate                            $response.json().get("responseCode")
    should be equal                             ${responseCode}                     200
    ${jobId}=                                   evaluate                            $response.json().get("jobDetail").get("jobId")
    log to console                              \n==============================================================================\n
    log to console                              CREATED JOB ID : ${jobId}
    Set Global Variable                         ${jobId}


TC2_DRIVER_ACCEPT_MISSION
    Create Session      accept_mission          ${base_url_snp}                      verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${response}=        POST On Session         accept_mission                      /skootar_ws_dev_v1/android_ws/v2/mission/accept/${jobId}/${skootarId}/1/1/13.${RANDOM_LAT}/100.${RANDOM_LNG}       headers=${headers}
    ${response.status_code}                     Convert To String                   ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    log to console                              body : ${response.content}
    ${responseCode}=                            evaluate                            $response.json().get("code")
    ${responseCode}                             Convert To String                   ${responseCode}
    ${responsedescTH}=                          evaluate                            $response.json().get("desc")
    should be equal                             ${responseCode}                     200
    should be equal                             ${responsedescTH}                   รับงานสำเร็จ


TC2_DRIVER_COMPLETE_MISSION
    Create Session      complete_mission        ${base_url_snp}                      verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${response}=        POST On Session         complete_mission                    /skootar_ws_dev_v1/android_ws/v3/mission/complete/${jobId}/${skootarId}/1/13.${RANDOM_LAT}/100.${RANDOM_LNG}/0       headers=${headers}
    ${response.status_code}                     Convert To String                   ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    log to console                              body : ${response.content}
    ${responseCode}=                            evaluate                            $response.json().get("code")
    ${responseCode}                             Convert To String                   ${responseCode}
    ${responsedescTH}=                          evaluate                            $response.json().get("desc")
    should be equal                             ${responseCode}                     200
    should be equal                             ${responsedescTH}                   ปิดงานเรียบร้อยแล้ว



